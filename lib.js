// Écrire la fonction JavaScript show Hx Content permettant
// d'afficher le contenu du div correspondant au titre
// sur lequel on clique.
function showHxContent(nm) {
    let title = document.getElementById('title' + nm);
    title.style.display = 'block';
}

//Question 2
// Écrire la fonction JavaScript hide Divs permettant de masquer
// le contenu de tous les div du documen
let hideAllDivs = () => {
    let divs = document.querySelectorAll('div');
    divs.forEach(element => {
        element.style.display = 'none';
    })
};
let alertTitle = () => {
    let GetTitle = Number(document.getElementById('title').value);
    alert('Titre n°: ' + GetTitle);
    let listButtonGet = document.querySelectorAll("button");
    let nm_item = GetTitle - 1;
    if (listButtonGet.item(nm_item).hasChildNodes()) {
        let item = listButtonGet.item(nm_item);
        alert(item.innerText);
    }else {
        alert('Il n\'y a pas de nom avec un numéro ' + GetTitle);
    }};
let deleteTitle  = () => {
    let GetTitle = Number(document.getElementById('delete_title').value);
    let listButtonGet = document.querySelectorAll("button");
    let nm_item = GetTitle - 1;
    if (listButtonGet.item(nm_item).hasChildNodes()) {
        let item = listButtonGet.item(nm_item);
        item.firstChild.remove();
    }else {
        alert('Il n\'y a pas de nom avec un numéro ' + GetTitle);
    }
};

let defineTitle   = () => {
    let listButtonGet = document.querySelectorAll("button");
    let nm = document.getElementById("title").value;
    let nm_item = nm - 1;
    if (listButtonGet.item(nm_item).hasChildNodes()) {
        let item = listButtonGet.item(nm_item);
        item.firstChild.remove();
    }
    listButtonGet.item(nm_item).innerText = 'Nouveau titre';
};